import { app, BrowserWindow, ipcMain } from 'electron'
const net = require('net')

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = require('path').join(__dirname, 'statics').replace(/\\/g, '\\\\')
}

let mainWindow

'use strict';

var os = require('os');
var ifaces = os.networkInterfaces();

Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }
      var ip = iface.address.split('.')
      var PORT = 6024;
      var HOST = (ip[0] + "." + ip[1] + "." + ip[2] + ".255")
      // Creating a datagram
      var dgram = require('dgram')
      var message = new Buffer.from('ip address')

      console.log('Sending Broadcast...')

      // Creating a UDP socket
      var client = dgram.createSocket('udp4')
      // Sending the broadcast
      function broadcast(){
        client.send(message, 0, message.length, PORT, HOST, function() {
          console.log('UDP message sent to ' + HOST + ':' + PORT)
        })
      }
      setInterval(broadcast, 20*100)
  });
})


// Connections array
// Contains:
//    tallyID(int): the ID of the connected tally
//    tallyName(string): the name of the connected tally
//    socket(object): The socket object of the tally
//    tallyCamNum(int): The camera number the tally has
let connections = []

// Create a socket server
let server = new net.createServer(connection, 2)

// When a new connection is made declare events and store the connections
// (Object) socket: the connection object
function connection (socket) {
  console.log('A new connection has been made!')

  // Event called when a new message is received
  socket.on('data', (data) => {
    let tallyData = data.toString('ascii').split('-')
    let tallyID = tallyData[0] // Extract the Tally name from the message
    let tallyName = tallyData[1] // Extract the Tally name from the message
    let tallyCamNum = tallyData[2] // Extract the live number of the tally
    console.log(tallyName + ' has connected')
    // mainWindow.webContents.send('NewConnection', tallyID, tallyName, tallyCamNum)
    connections.push({ tallyID, tallyName, socket, tallyCamNum }) // Save connection and data
    mainWindow.webContents.send('SyncConnections', connections)
  })

  // Event called when an error is thrown
  socket.on('error', (data) => {
    console.log('En error has occured')
    console.log(data)
  })
  // Event called when client starts disconnecting
  socket.on('end', () => {
    console.log('Ending connection')
    removeClient(socket)
  })
  // Event called when a connection is closed as result of an error
  socket.on('close', () => {
    console.log('Closing connetion')
    removeClient(socket)
  })

  socket.write('2') // Ask tally for information
}

// Remove client from client list
// (object)socket: the client that has to be removed
function removeClient (socket) {
  for (let connection of connections) {
    if (connection.socket === socket) {
      connections.splice(connections.indexOf(connection))
    }
  }
  mainWindow.webContents.send('SyncConnections', connections)
}

// Send a message to all the connected tallys
// msg(string): the message that needs to be send
function sendToAll (msg) {
  for (let connection of connections) {
    connection.socket.write(msg)
  }
}

// Send command to a tally
// command(int/string): the command that needs to be send
// data(string): the command data
// tallyName(string) the name of the tally that needs to receive the command
function sendCommandTo (command, data, tallyID) {
  for (let tallyLight of connections) {
    if (tallyLight.tallyID === tallyID) {
      let msg = command + data
      tallyLight.socket.write(msg)
    }
  }
}

server.on('error', (err) => {
  throw err
})
server.listen(8124, () => { // Start listening for clients
  console.log('server bound')
})

// Send the changed camera number
// Data (int): the camera that is live
ipcMain.on('change', (event, data) => {
  console.log('change: ' + data)
  let msg = '1' + data
  sendToAll(msg)
})

// Change the camera number of a specific tally
// data (object): the object holding the tally name and socket
ipcMain.on('changeCamNum', (event, data) => {
  sendCommandTo(3, data.tallyName, data.tallyID)
})

// Change the camera number and name of a specific tally
// data (object): the object holding the tally name and socket
ipcMain.on('changeTallyData', (event, data) => {
  sendCommandTo(3, '-' + data.tallyCamNum + '-' + data.tallyName, data.tallyID)
})

// Forget the wifi network of the tally
// data (object): the object holding the tally name and socket
ipcMain.on('forgetWifi', (event, data) => {
  sendCommandTo(4, '', data.tallyID)
})

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    minWidth: 700,
    minHeight: 500,
    useContentSize: true,
    icon: require('path').join(__dirname, 'statics/icons/camera.png').replace(/\\/g, '\\\\')
  })

  mainWindow.loadURL(process.env.APP_URL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
